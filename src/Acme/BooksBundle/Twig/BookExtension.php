<?php
namespace Acme\BooksBundle\Twig;

class BookExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('imageResize', array($this, 'imageResizeFunction'), array('is_safe' => array('html'))),
        );
    }

    public function imageResizeFunction($image, $width = 100, $height = 100)
    {
        return "<img src='$image' width='$width' height='$height'/>";
    }

    public function getName()
    {
        return 'book_extension';
    }
}
