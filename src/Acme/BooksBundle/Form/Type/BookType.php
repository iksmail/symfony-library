<?php

namespace Acme\BooksBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text')
            ->add('author', 'text')
            ->add('cover_file', 'file')
            ->add('bookfile_file', 'file')
            ->add('dateread', 'datetime')
            ->add('downloadable', 'checkbox')
            ->add('use_cover', 'hidden', array('mapped' => false, 'data' => 1))
            ->add('use_bookfile', 'hidden', array('mapped' => false, 'data' => 1))
            ->add('Submit', 'submit');
    }

    public function getName()
    {
        return 'book';
    }
}
