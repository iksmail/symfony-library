<?php

namespace Acme\BooksBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Book
 *
 * @ORM\Table()
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"isFileUploadedOrExists"})
 */
class Book
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="cover", type="string", length=255, nullable=true)
     */
    private $cover;

    /**
     * @var string
     *
     * @ORM\Column(name="bookfile", type="string", length=255, nullable=true)
     */
    private $bookfile;

    /**
     * @var string
     *
     * @ORM\Column(name="bookfile_realname", type="string", length=255, nullable=true)
     */
    private $bookfile_realname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateread", type="datetime")
     */
    private $dateread;

    /**
     * @var boolean
     *
     * @ORM\Column(name="downloadable", type="boolean")
     */
    private $downloadable;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * @var string
     */
    private $cover_file;

    /**
     * @Assert\File(maxSize="5000000")
     */
    private $bookfile_file;

    /**
     * @var string
     */
    private $cover_temp;

    /**
     * @var string
     */
    private $bookfile_old;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     * @param  string $title
     * @return Book
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set author
     * @param  string $author
     * @return Book
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set cover
     * @param  string $cover
     * @return Book
     */
    public function setCover($cover)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set bookfile
     * @param  string $bookfile
     * @return Book
     */
    public function setBookfile($bookfile)
    {
        $this->bookfile = $bookfile;

        return $this;
    }

    /**
     * Get bookfile
     * @return string
     */
    public function getBookfile()
    {
        return $this->bookfile;
    }

    /**
     * @param  string $bookfile_realname
     * @return Book
     */
    public function setBookfileRealname($bookfile_realname)
    {
        $this->bookfile_realname = $bookfile_realname;

        return $this;
    }

    /**
     * @return string
     */
    public function getBookfileRealname()
    {
        return $this->bookfile_realname;
    }

    /**
     * @param  \DateTime $dateread
     * @return Book
     */
    public function setDateread(\DateTime $dateread)
    {
        $this->dateread = $dateread;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateread()
    {
        return $this->dateread;
    }

    /**
     * Set downloadable
     *
     * @param  boolean $downloadable
     * @return Book
     */
    public function setDownloadable($downloadable)
    {
        $this->downloadable = $downloadable;

        return $this;
    }

    /**
     * Get downloadable
     * @return boolean
     */
    public function getDownloadable()
    {
        return $this->downloadable;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setCoverFile(UploadedFile $file = null)
    {
        $this->cover_file = $file;
        if (isset($this->cover)) {
            $this->cover_temp = $this->cover;
            $this->cover = null;
        } else {
            $this->cover = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getCoverFile()
    {
        return $this->cover_file;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setBookfileFile(UploadedFile $file = null)
    {
        $this->bookfile_file = $file;
        if (isset($this->bookfile)) {
            $this->bookfile_old = $this->bookfile;
            $this->bookfile = null;
        } else {
            $this->bookfile = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getBookfileFile()
    {
        return $this->bookfile_file;
    }

    /**
     * @return string
     */
    public function getBookfileOld()
    {
        return $this->bookfile_old;
    }

    /**
     * @param $updated_at
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web'.$this->getUploadDir();
    }

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        return '/uploads/books';
    }

    /**
     * @param  string $file
     * @return string
     */
    protected function getFileWebPath($file)
    {
        $level[0] = substr($file, 0, 2);
        $level[1] = substr($file, 2, 2);
        $level[2] = substr($file, 4, 2);
        $path = $this->getUploadDir().'/'.implode('/', $level);

        return $path.'/';
    }

    /**
     * @param  string $file
     * @return string
     */
    protected function getFileAbsolutePath($file)
    {
        $level[0] = substr($file, 0, 2);
        $level[1] = substr($file, 2, 2);
        $level[2] = substr($file, 4, 2);
        $path = $this->getUploadRootDir().'/'.implode('/', $level);

        return $path.'/';
    }

    /**
     * @return string
     */
    public function getCoverWebPath()
    {
        if (!$this->cover) return null;
        return $this->getFileWebPath($this->cover) . $this->cover;
    }

    /**
     * @return string
     */
    public function getBookfileWebPath()
    {
        if (!$this->bookfile) return null;
        return $this->getFileWebPath($this->bookfile) . $this->bookfile;
    }

    /**
     * @return null|string
     */
    public function getCoverAbsolutePath()
    {
        if (!$this->cover) return null;
        return $this->getFileAbsolutePath($this->cover) . $this->cover;
    }

    /**
     * @return null|string
     */
    public function getBookfileAbsolutePath()
    {
        if (!$this->bookfile) return null;
        return $this->getFileAbsolutePath($this->bookfile) . $this->bookfile;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        $this->updated_at = new \DateTime();

        if (null !== $this->getCoverFile()) {
            $filename = md5(uniqid(mt_rand(), true));
            $this->cover = $filename.'.'.$this->getCoverFile()->guessExtension();
        }
        if (null !== $this->getBookfileFile()) {
            $filename = md5(uniqid(mt_rand(), true));
            $this->bookfile = $filename.'.'.$this->getBookfileFile()->guessExtension();
            $this->bookfile_realname = $this->getBookfileFile()->getClientOriginalName();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null !== $this->getBookfileFile()) {
            $this->getBookfileFile()->move($this->getFileAbsolutePath($this->bookfile), $this->bookfile);
            if (isset($this->bookfile_old)) {
                @unlink($this->getFileAbsolutePath($this->bookfile_old) . $this->bookfile_old);
                $this->bookfile_old = null;
            }
            $this->bookfile_file = null;
        }
        if (null !== $this->getCoverFile()) {
            $this->getCoverFile()->move($this->getFileAbsolutePath($this->cover), $this->cover);
            if (isset($this->cover_temp)) {
                @unlink($this->getFileAbsolutePath($this->cover_temp) . $this->cover_temp);
                $this->cover_temp = null;
            }
            $this->cover_file = null;
        }
    }

    /**
     * @param ExecutionContextInterface $context
     */
    public function isFileUploadedOrExists(ExecutionContextInterface $context)
    {
        if(null === $this->bookfile && null === $this->bookfile_file)
            $context->addViolationAt('bookfile_file', 'You have to choose a file', array(), null);
    }
}
