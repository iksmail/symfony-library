<?php

namespace Acme\BooksBundle\Controller;

use Acme\BooksBundle\Entity\Book;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Acme\BooksBundle\Form\Type\BookType;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $user = $this->getUser();

        /** @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getDoctrine()->getManager();

        $last_modified = $em->createQuery('select max(p.updated_at) from AcmeBooksBundle:Book p')->getSingleScalarResult();
        $datetime = new \DateTime($last_modified);
        $count = $em->createQuery('select count(p.id) from AcmeBooksBundle:Book p')->getSingleScalarResult();
        $response = new Response();

        $response->setPrivate();
        $response->setCache(array(
            'last_modified' => $datetime,
            'max_age' => 86400,
            'etag' => md5(($user?'1':'0').$count.$datetime->format('U'))
        ));

        if ($response->isNotModified($request)) {
            return $response;
        }

        $query = $em->createQuery('select p from AcmeBooksBundle:Book p order by p.dateread desc');
        $books = $query->getResult();

        $response->setContent($this->renderView('AcmeBooksBundle:Default:index.html.twig', array('books' => $books)));

        return $response;
    }

    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($id = $request->get('id')) {
            $book = $em->getRepository('AcmeBooksBundle:Book')->find($id);
            if (!$book) throw new NotFoundHttpException('Книга не найдена');
        } else
            $book = new Book();

        $form = $this->createForm(new BookType(), $book);

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var Book $book */
            $book = $form->getData();

            if (!$form->get('use_cover')->getData()) $book->setCover(null);
            if (!$form->get('use_bookfile')->getData()) $book->setBookfile(null);

            $book->preUpload();
            $em->persist($book);
            $em->flush();

            return $this->redirect($this->generateUrl('acme_books_homepage'));
        }

        return $this->render('AcmeBooksBundle:Default:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function downloadAction($id)
    {
        /** @var Book $book */
        $book = $this->getDoctrine()->getRepository('AcmeBooksBundle:Book')->find($id);
        if (!$book) {
            throw new NotFoundHttpException('Книга не найдена');
        }
        $file = new BinaryFileResponse($book->getBookfileAbsolutePath());
        $file->setContentDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $book->getBookfileRealname(), uniqid());

        return $file;
    }

    public function deleteCoverAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Book $book */
        $book = $em->getRepository('AcmeBooksBundle:Book')->find($id);
        if (!$book) {
            throw new NotFoundHttpException('Книга не найдена');
        }
        if ($book->getCover()) {
            $book->setCover(null);
            $em->flush();

            $file = $book->getAbsolutePath().$book->getCover();
            if ($file && is_file($file)) unlink($file);
        }

        return $this->redirect($this->generateUrl('acme_books_edit', array('id' => $id)));
    }

    public function deleteBookfileAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Book $book */
        $book = $em->getRepository('AcmeBooksBundle:Book')->find($id);
        if (!$book) {
            throw new NotFoundHttpException('Книга не найдена');
        }
        if ($book->getBookfile()) {
            $book->setBookfile(null)->setBookfileRealname(null);
            $em->flush();

            $file = $book->getAbsolutePath().$book->getBookfile();
            if ($file && is_file($file)) unlink($file);
        }

        return $this->redirect($this->generateUrl('acme_books_edit', array('id' => $id)));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Book $book */
        $book = $em->getRepository('AcmeBooksBundle:Book')->find($id);
        if (!$book) {
            throw new NotFoundHttpException('Книга не найдена');
        }

        $em->remove($book);
        $em->flush();

        return $this->redirect($this->generateUrl('acme_books_homepage'));
    }
}
