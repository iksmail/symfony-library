<?php
// application.php

//require('vendor/autoload.php');
$loader = require_once __DIR__.'/../app/bootstrap.php.cache';

use Acme\Console\Command\GreetCommand;
use Acme\DemoBundle\Command\HelloWorldCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new GreetCommand);
$application->add(new HelloWorldCommand());
$application->run();
