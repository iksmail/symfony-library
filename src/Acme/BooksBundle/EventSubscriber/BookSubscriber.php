<?php

namespace Acme\BooksBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber as DoctrineEventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class BookSubscriber implements DoctrineEventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'preUpdate',
            'onFlush',
        );
    }

    public function preUpdate(PreUpdateEventArgs $event)
    {
        /** @var \Acme\BooksBundle\Entity\Book $entity */
        $entity = $event->getEntity();
        if ($event->hasChangedField('cover') && $event->getNewValue('cover') === null) {
            $entity->setCover($event->getOldValue('cover'));
            $file = $entity->getCoverAbsolutePath();
            $entity->setCover($event->getNewValue('cover'));
            if ($file && file_exists($file) && is_file($file))
                unlink($file);
        }
        if ($event->hasChangedField('bookfile') && $event->getNewValue('bookfile') === null) {
            $entity->setBookfile($event->getOldValue('bookfile'));
            $file = $entity->getBookfileAbsolutePath();
            $entity->setBookfile($event->getNewValue('bookfile'));
            if ($file && file_exists($file) && is_file($file))
                unlink($file);
        }
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            $file = $entity->getCoverAbsolutePath();
            if ($file && file_exists($file) && is_file($file))
                unlink($file);
            $file = $entity->getBookfileAbsolutePath();
            if ($file && file_exists($file) && is_file($file))
                unlink($file);
        }
    }
}
